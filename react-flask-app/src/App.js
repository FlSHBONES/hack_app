import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';

function App() {
  var message = "";
  var [response, setResponse] = useState("")
  var [inputResponse, setInputResponse] = useState("")
  useEffect(() => {
    fetch(`/translate/${inputResponse}`).then(res => res.json()).then(data => {
      setResponse(data);
      console.log(data);
      console.log(response);
    });
  }, []);

  function getInput() {
    fetch(`/translate/${inputResponse}`).then(res => res.json()).then(data => {
      setResponse(data);
      console.log(data);
      console.log(response);
    });
  }
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Zoomer to Boomer</h1>
      </header>
      <body className="Body">
        <label for="Input">What's the youth sayin today?</label>
        <input type="text" id="input" placeholder= "Enter something here..." value={inputResponse} onChange={e => setInputResponse(e.target.value)}></input>
        <button onClick={getInput}>Translate</button>;

        <p className="Translation_header">Translating {message} into boomer talk....</p>
        {Object.entries(response).map(([k, v]) =>
        <div className="Translation_body">
          <h5>{k}</h5>
          <div>{v}</div>
        </div>)}
      </body>
    </div>
  );
}

export default App;
