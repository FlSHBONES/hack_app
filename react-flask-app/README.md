# Getting Started with GenZ Translator 3000

## Clone Repository
    run

<git clone https://FlSHBONES@bitbucket.org/FlSHBONES/hack_app.git>

    to clone repository

## Download dependancies
    1. 
    run 

<cd ./hack_app/react-flask-app/api/venv>

    to move to the proper directory

    2. 
    now run

<python -m venv ./.venv/
    source ./.venv/bin/activate
    pip install -f requirements.txt>
    <npm install>

    to install dependancies

## Run locally

    ###1. RUN SERVER

    run 

<flask run>
    
    This will run the back-end server on port 5000

    ###2. RUN FRONT-END

    run

<cd ../..>
<yarn start>

##USE

    Now enter a message into the text box and press 'Translate' to have all slang
    definitions provided below