import requests
import json
from flask import Flask, request
from better_profanity import profanity
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

stopwords = ('ourselves', 'hers', 'between', 'yourself', 'but', 'again', 'there', 'about', 'once', 'during', 'out', 'very', 'having', 'with', 'they', 'own', 'an', 'be', 'some', 'for', 'do', 'its', 'yours', 'such', 'into', 'of', 'most', 'itself', 'other', 'off', 'is', 's', 'am', 'or', 'who', 'as', 'from', 'him', 'each', 'the', 'themselves', 'until', 'below', 'are', 'we', 'these', 'your', 'his', 'through', 'don', 'nor', 'me', 'were', 'her', 'more', 'himself', 'this', 'down', 'should', 'our', 'their', 'while', 'above', 'both', 'up', 'to', 'ours', 'had', 'she', 'all', 'no', 'when', 'at', 'any', 'before', 'them', 'same', 'and', 'been', 'have', 'in', 'will', 'on', 'does', 'yourselves', 'then', 'that', 'because', 'what', 'over', 'why', 'so', 'can', 'did', 'not', 'now', 'under', 'he', 'you', 'herself', 'has', 'just', 'where', 'too', 'only', 'myself', 'which', 'those', 'i', 'after', 'few', 'whom', 't', 'being', 'if', 'theirs', 'my', 'against', 'a', 'by', 'doing', 'it', 'how', 'further', 'was', 'here', 'than')

@app.route('/translate/<message>')

# Function which iterates through each word in a message and returns a dictionary of lists
# containing the top definition for each word
# @return: returns a dictionary - keys: words in message, values: definitions

def translate(message = "Yo is arya hard capping rn smh"):
    try:

        # Message to be translated

        #message = "Yo arya is hard capping rn"
        
        # Dictionary to be populated with definitions
        # The first key value pair is an introduction message

        term_dict = {}

        # For each word in the message, get the definition and add it to the dictionary
        
        for word in message.split():
            if(word not in stopwords):
                term_dict[word] = profanity.censor(define(word))
                #term_dict[word] = define(word)

        print(term_dict)
        return term_dict
    except:
        return None
        

# Function which connects to API and fetches definition for a given term
# @param term: the term to be defined
# @return: returns a string 

def define(term):
    try:

        definition = 'Not Slang'

        # Connecting to the Urban Dictionary API

        url = "https://mashape-community-urban-dictionary.p.rapidapi.com/define"

        querystring = {"term":term}

        headers = {
            'x-rapidapi-host': "mashape-community-urban-dictionary.p.rapidapi.com",
            'x-rapidapi-key': "a921a2427bmsha52bd93eab98958p140ec3jsn5982633bade9"
            }
            
        # Attempt connection

        response = requests.request("GET", url, headers=headers, params=querystring)

        term_dict = json.loads(response.text)

        term_entries = term_dict["list"]

        max_upvotes = 100


        for entry in term_entries:

            if(entry['thumbs_up'] > max_upvotes):
                definition = entry['definition']
                max_upvotes = entry['thumbs_up']

        return definition
    except:
        pass
    return None

def hello():
    return 'Hello world!'