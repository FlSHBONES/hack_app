## Getting Started with GenZ Translator 3000

# Clone Repository
    run <git clone https://FlSHBONES@bitbucket.org/FlSHBONES/hack_app.git>
    to clone repository
    
    Make sure git is installed

# Clone Repo
    
    1. Enter your deired repo and clone using the link bitbucket provides
    
# Download dependancies
    1. 
    run 

<cd ./hack_app/react-flask-app/api/venv>

    to move to the proper directory

    2.
    
    now run

<source bin/activate>

<pip install -r requirements.txt>

    to install dependancies

# Run locally

    RUN SERVER

    run 

    <flask run>
    
    This will run the back-end server on port 5000

    RUN FRONT-END
    
    On a seperate terminal, navigate to the path < cd./hack_app/react-flask-app>
    
    
    run to install yarn
    
    <curl -sL https://dl.yarnpkg.com/rpm/yarn.repo -o /etc/yum.repos.d/yarn.repo>
    
    <sudo yum install yarn>
    
    run to start front-end
    
    <yarn start>

    USE

    Now enter a message into the text box and press 'Translate' to have all slang
    definitions provided below
    
    Here are some example sentences:
    "lmao you wanna scrap or nah"
    "deadass you is hard capping rn smh"
    "Homie sizing-me-up?"